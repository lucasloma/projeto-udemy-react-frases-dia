import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Button,
    Alert
} from 'react-native';
import { WebBrowser } from 'expo';


export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
          <ScrollView style={styles.principal}>

              <Text style={styles.headerpLinkText}>Frases do Dia</Text>

              <Image
                 source={require('../assets/images/magic.png')}
                 style={styles.Imagem}/>

              <Button
                  title='Frase'
                  color='pink'
                  onPress={onPress}/>

          </ScrollView>
          <View style={styles.helpContainer}>
              <TouchableOpacity onPress={this._handleHelpPress} style={styles.helpLink}>
                  <Text style={styles.helpLinkText}>
                      -): Meu Portifólio!</Text>
              </TouchableOpacity>
          </View>
      </View>
    );
  }


    _handleHelpPress = () => {
        WebBrowser.openBrowserAsync(
            'http://lucaslobatomaciel.com/lucaslobatomaciel/blogLucas/web/'
        );
    };

}

const FRASES =
    [
        "Por outro lado, a consolidação das estruturas exige a precisão e a definição do sistema de formação de quadros que corresponde às necessidades.",
        "O avanço tecnológico, assim como o entendimento das metas propostas, promovem alavancagem dos índices pretendidos.",
        "As experiências acumuladas demonstram que a valorização de fatores subjetivos aponta para a melhoria das direções preferenciais no sentido do progresso.",
        "Gostaria de enfatizar que a percepção das dificuldades assume importantes posições no estabelecimento do processo de comunicação como um todo.",
        "As experiências acumuladas demonstram que o desenvolvimento contínuo de distintas formas de atuação não pode mais se dissociar do levantamento das variáveis envolvidas.",
        "O que temos que ter sempre em mente é que o aumento do diálogo entre os diferentes setores produtivos promove a alavancagem do fluxo de informações.",
        "Todavia, a valorização de fatores subjetivos oferece uma interessante oportunidade para verificação das formas de ação.",
        "Nunca é demais lembrar o peso e o significado destes problemas, uma vez que a estrutura atual da organização exige a precisão e a definição das condições financeiras e administrativas exigidas.",
        "É claro que a constante divulgação das informações aponta para a melhoria do retorno esperado a longo prazo.",
        "Gostaria de enfatizar que o acompanhamento das preferências de consumo acarreta um processo de reformulação e modernização das direções preferenciais no sentido do progresso.",
        "A prática cotidiana prova que a competitividade nas transações comerciais facilita a criação do orçamento setorial.",
        "O empenho em analisar o início da atividade geral de formação de atitudes auxilia a preparação e a composição dos níveis de motivação departamental.",
        "Neste sentido, a expansão dos mercados mundiais pode nos levar a considerar a reestruturação do sistema de formação de quadros que corresponde às necessidades.",
        "No entanto, não podemos esquecer que a contínua expansão de nossa atividade é uma das consequências do sistema de formação de quadros que corresponde às necessidades.",
        "A prática cotidiana prova que a percepção das dificuldades prepara-nos para enfrentar situações atípicas decorrentes do remanejamento dos quadros funcionais.",
        "Evidentemente, a determinação clara de objetivos garante a contribuição de um grupo importante na determinação dos paradigmas corporativos.",
        "Nunca é demais lembrar o peso e o significado destes problemas, uma vez que o comprometimento entre as equipes agrega valor ao estabelecimento do orçamento setorial."
    ];

function onPress() {
    numeroAleatorio =  Math.random();
    numeroAleatorio = Math.floor(numeroAleatorio *15);
    frases = FRASES[numeroAleatorio];
    Alert.alert('-(:', frases);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent:'center',
        alignItems:'center'
    },
    principal: {
        paddingTop: 40
    },
    botao:{
      marginTop:40
    },
    Imagem:{
        aspectRatio:1,
        resizeMode:"cover",
        margin:20
    },
    helpContainer: {
        marginTop: 15,
        //alignItems: 'center',
    },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
    headerpLinkText: {
        fontSize: 20,
        color: '#2e78b7',
        marginBottom:40
    },
});
